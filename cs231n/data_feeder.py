"""File: datafeeder.py
Author: galileilei
Email: galileilei@gmail.com
Description:
    Helper Class for reading in batches of data.
"""
import numpy as np
from .data_utils import get_CIFAR10_data

class Batch:
    """
    Helper class to divide data into batches
    """
    def __init__(self, lst, batch_size=64):
        """
        :params lst: a numpy array
        :params batch_size: the size of each batch
        """
        self.size = len(lst)
        self.batch_size = batch_size
        self.idx = 0
        # append the data so that all batch have the same size
        temp = list(lst.shape)
        temp[0] += batch_size
        self.data = np.zeros(tuple(temp))
        self.data[:self.size] = lst
        self.data[self.size:] = lst[:self.batch_size]

    def __iter__(self):
        """
        Declare itself as iterable, so that
        for b in Batch(data):
            do_something(b)
        works
        """
        self.idx = 0
        return self

    def __next__(self):
        """
        required method for iterable class
        """
        i, b, N = self.idx, self.batch_size, self.size
        if i >= N:
            raise StopIteration
        else:
            self.idx += b
            return self.data[i:(i + b)]

    def next_batch(self):
        """
        Deprecated method. use iterable instead.
        Loops over the data once.
        """
        T = int(self.size/self.batch_size)
        i, b= self.idx, self.batch_size
        for t in range(T):
            yield self.data[i:(i + b)]
            i += b
        yield self.data[i:]

    def clear(self):
        self.idx = 0

class CifarDataFeeder:
    def __init__(self, batch_size=64, total_size=None):
        data_dict = get_CIFAR10_data()
        self.data = dict()
        for k in data_dict:
            if total_size is None:
                self.data[k] = Batch(data_dict[k], batch_size)
            else:
                self.data[k] = Batch(data_dict[k][:total_size], batch_size)

    def next_batch(self, mode="training"):
        """
        Deprecated. use get_data() instead.
        """
        if mode == "testing":
            for x, y in zip(self.data['X_test'].next_batch(), self.data['y_test'].next_batch()):
                yield (x, y)
        elif mode == "training":
            for x, y in zip(self.data['X_train'].next_batch(), self.data['y_train'].next_batch()):
                yield (x, y)
        elif mode == "validating":
            for x, y in zip(self.data['X_val'].next_batch(), self.data['y_val'].next_batch()):
                yield (x, y)

    def get_data(self, mode="training"):
        """
        example usage:
        data = CifarDataFeeder(batch_size=64)
        for bx, by in data.get_data("training"):
            f(bx, by)
        """
        if mode == "testing":
            return self.data['X_test'], self.data['y_test']
        if mode == "training":
            return self.data['X_train'], self.data['y_train']
        if mode == "validating":
            return self.data['X_val'], self.data['y_val']

    def clear(self, mode="training"):
        """
        Deprecated. use get_data() instead.
        """
        if mode == "testing":
            self.data['X_test'].clear()
            self.data['y_test'].clear()
        elif mode == "training":
            self.data['X_train'].clear()
            self.data['y_train'].clear()
        elif mode == "validating":
            self.data['X_val'].clear()
            self.data['y_val'].clear()
