"""
File: cnn.py
Author: Me
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    Implement a Convolution Neural Network
"""
import tensorflow as tf
import numpy as np

import tflearn
from tflearn.layers.conv import conv_2d, max_pool_2d
from tflearn.layers.core import fully_connected
from tflearn.layers.normalization import batch_normalization

def default_parameters(layer_type="conv2d"):
    """
    returns a dictionary which describe a 'conv2d' or 'linear' layer.
    """
    result = {}
    if layer_type == "conv2d":
        result = {'filter' : [5, 5, 32, 32], 'stride' : [1, 1, 1, 1], 'padding' : 'SAME', \
                'data_format' : 'NHWC', 'bias' : [32], 'max_pool': [1, 2, 2, 1], 'input': None, \
                'use_bn' : True, 'output' : None}
        if layer_type == "linear":
            result = {'weight' : [8 * 8 * 32, 10], 'bias': [10], 'input' : None}
    return result

class HyperParameter:
    """
    Stores all parameters related to CNN.
    """
    def __init__(self, _param, _sorted):
        """
        :param _param : _param[layer_name] = dict() with all infos
        :param _sorted: _sorted = [] is a topologically sorted list of layer_name

        example:
        Suppose the network is a two layer CNN follows by one affine layer:
        'layer_1' -> 'layer_2' -> 'linear'

        Then we declare it as:
        hyperparam = dict()
        hyperparam['layer_1'] = default_parameters("conv2d")
        hyperparam['layer_1']['filter'] = [5, 5, 3, 32] # to be used with cifar-10 data
        hyperparam['layer_2'] = default_parameters("conv2d")
        hyperparam['layer_2']['input'] = 'layer_1'
        hyperparam['linear'] = default_parameters("linear")
        hyperparam['linear']['input'] = 'layer_2'

        cnn_param = HyperParameter(hyperparam, ['layer_1', 'layer_2'])
        cnn = CNN(cnn_param)
        """
        self.parameters = _param
        self.layers_topo_sorted = _sorted
        pass

    def topology_sort(self):
        return self.layers_topo_sorted

    def get(self, i):
        return self.parameters[i]

    def num_layers():
        return len(self.layers_topo_sorted)

class CNN:
    def __init__(self, _params=None, _graph=None, _sess=None):
        """
        :param _params: a HyperParameter() object
        :param _graph: a tensorflow.Graph() object

        note that no tensorflow.Session() or number of epoches are needed here;
        this merely declares the computation graph.
        """
        self.params = _params
        self.graph = tf.Graph() if _graph is None else _graph
        #self.session = tf.Session(graph=self.graph) if _sess is None else _sess
        #self.num_epochs = 2
        self.layer = dict()
        self._build_network(method='own')

    def conv2d(self, net, parameter, scope, method="tflearn"):
        if method == "tflearn":
            return self._tflearn_conv2d(net, parameter, scope)
        else:
            return self._own_conv2d(net, parameter, scope)

    def _tflearn_conv2d(self, net, parameter, scope):
        """
        :param net: tensorflow.Operation() object
        :param parameter: dict() object with key:value that control convolution
        :scope scope: tensorflow.variable_scope()

        if x = net, then it returns tf.bn(tf.relu(tf.conv2d(x, W, b)))
        """
        with self.graph.as_default(), tf.variable_scope(scope):
            f, s, p = parameter['filter'], parameter['stride'], parameter['padding']
            h, w, in_dim, out_dim = f
            post_activation = conv_2d(net, out_dim, [h, w], strides=s, activation='relu', regularizer='L2')
            if parameter['use_bn']:
                batch_norm = batch_normalization(post_activation)
            else:
                batch_norm = post_activation
            # max pool. this seems to be the optimal order
            mp = parameter['max_pool'][1:3]
            if mp is None:
                return batch_norm
            else:
                result = max_pool_2d(batch_norm, mp)
                return result

    def _own_conv2d(self, net, parameter, scope):
        """
        :param net: tensorflow.Operation() object
        :param parameter: dict() object with key:value that control convolution
        :scope scope: tensorflow.variable_scope()

        if x = net, then it returns tf.bn(tf.relu(tf.conv2d(x, W, b)))
        """
        with self.graph.as_default(), tf.variable_scope(scope):
            f, s, p = parameter['filter'], parameter['stride'], parameter['padding']
            d = parameter['data_format']
            b = parameter['bias']
            kernel = tf.get_variable('W', shape=f,\
                    initializer=tf.truncated_normal_initializer(stddev=5e-2))
            self.graph.add_to_collection("L2", kernel)
            bias = tf.get_variable('b', shape=b, initializer=tf.zeros_initializer())
            y = tf.nn.conv2d(net, kernel, s, p, data_format=d, name="convolution")
            pre_activation = tf.nn.bias_add(y, bias, name="pre_activation")
            post_activation = tf.nn.relu(pre_activation, name="post_activation")
            if parameter['use_bn']:
                batch_norm = tf.layers.batch_normalization(post_activation, training=tflearn.get_training_mode(), name=scope)
            else:
                batch_norm = post_activation
            mp = parameter['max_pool']
            if mp is None:
                return batch_norm
            else:
                result = tf.nn.max_pool(batch_norm, mp, mp, p, name="max_pool")
                return result

    def linear(self, net, param, scope, method="own"):
        if method == "own":
            return self._own_linear(net, param, scope)
        else:
            return self._tflearn_linear(net, param, scope)

    def _tflearn_linear(self, net, param, scope):
        """
        see conv2d() for parameter explanation.

        if x = net, it returns tf.matmul(net, W) + b
        """
        with self.graph.as_default(), tf.variable_scope(scope):
            # first linear layer
            y0 = fully_connected(net, 128, regularizer='L2', activation='relu')
            # second linear layer
            y1 = fully_connected(y0, 10, regularizer='L2', activation='linear')
            return y1

    def _own_linear(self, net, param, scope):
        """
        see conv2d() for parameter explanation.

        if x = net, it returns tf.matmul(net, W) + b
        """
        with self.graph.as_default(), tf.variable_scope(scope):
            # first linear layer
            reshape = tf.reshape(net, [64, -1])
            #dim = reshape.get_shape()[1].value
            dim = param['weight'][0]
            W0 = tf.get_variable('W0', shape=[dim, 192], initializer=tf.truncated_normal_initializer(stddev=5e-2))
            self.graph.add_to_collection("L2", W0)
            b0 = tf.get_variable('b0', shape=[192], initializer=tf.zeros_initializer())
            y0 = tf.nn.relu(tf.nn.bias_add(tf.matmul(reshape, W0), b0, name="bias_add_0"), name="post_activation_0")
            # second linear layer
            W1 = tf.get_variable('W1', shape=[192, 10], initializer=tf.truncated_normal_initializer(stddev=5e-2))
            self.graph.add_to_collection("L2", W1)
            b1 = tf.get_variable('b1', shape=[10], initializer=tf.zeros_initializer())
            y1 = tf.nn.bias_add(tf.matmul(y0, W1), b1, name="bias_add_1")
            return y1

    def concat_input(self, inputs):
        """
        return a 4D tensor concat along the last dimension.
        :param inputs: a list of 4D tensors, where shape[1] and shape[2] may not match.
        """
        print("inputs to concat_input() are {}".format(inputs))
        if not isinstance(inputs, list):
            return inputs
        all_h = [v.shape[1].value for v in inputs]
        all_w = [v.shape[2].value for v in inputs]
        output_h = np.max(all_h)
        output_w = np.max(all_w)
        padded_inputs = []
        for i, v in enumerate(inputs):
            h, w = v.shape[1].value, v.shape[2].value
            pad_h, pad_w = (output_h - h)/2, (output_w - w)/2
            padding = np.array([[0, 0], [pad_h, pad_h], [pad_w, pad_w], [0, 0]])
            padded_inputs.append(tf.pad(v, padding))
        result = tf.concat(padded_inputs, 3)
        return result

    def _build_network(self, method='tflearn'):
        """
        Declare all operations on the graph.
        Assume the last layer is linear.

        Returns a tuple of three tf.Operation() objects:
        1. softmax_linear: compute unscaled probability of classifier, in RR^k
        2. loss: computes cross entropy loss given true labels, in RR
        3. train_op: updates all trainable weights
        """
        params, graph = self.params, self.graph
        layers = dict()
        with graph.as_default(): # Important that only define for this graph
            #tflearn.init_training_mode()  # see tflearn/config.py training mode comment
            self.image = tf.placeholder(tf.float32, shape=[None, 32, 32, 3], name="input")
            for j, layer_name in enumerate(params.topology_sort()):
                inputs = params.get(layer_name)['input']
                if inputs is None:
                    input_tensor = self.image
                else:
                    input_tensor = self.concat_input([layers[x] for x in inputs])
                layers[layer_name] = self.conv2d(input_tensor, params.get(layer_name), scope=layer_name, method=method)
                layers["{}_anchor".format(layer_name)] = input_tensor
                outputs = params.get(layer_name)['output']
                if outputs is None:
                    params.get('linear')['input'].append(layer_name)
            #TODO: not yet tested
            inputs = params.get('linear')['input']
            input_tensor = self.concat_input([layers[x] for x in inputs])
            softmax_linear = self.linear(input_tensor, params.get('linear'), scope='linear', method=method)
            layers['linear'] = softmax_linear
            layers['linear_anchor'] = input_tensor
            self.label = tf.placeholder(tf.int64, shape=[None, ], name="target_label")
            one_hot_label = tf.one_hot(self.label, 10, name="one_hot_label")
            if method == 'own':
                loss2 = tf.reduce_mean(tf.nn.sparse_softmax_cross_entropy_with_logits(logits=softmax_linear, labels=self.label), name="nn_xentropy_loss")
                loss = tf.reduce_mean(tf.losses.softmax_cross_entropy(one_hot_label, softmax_linear), name="xentropy_loss")
                #l2_loss = self._l2_loss(loss)
                result, alpha = [], 1e-4
                for v in self.graph.get_collection("L2"):
                    result.append(alpha * tf.nn.l2_loss(v))
                print(result)
                l2_loss = loss2 + tf.add_n(result, name="regularized_weights")
                #train_op = self._build_train_op(l2_loss)
                # train_op = tf.train.RMSPropOptimizer(5e-4).minimize(loss)
                # (this works)
                train_op = tf.train.AdamOptimizer().minimize(l2_loss)
                layers['logit'], layers['l2_loss'], layers['optimizer'] = softmax_linear, l2_loss, train_op
                layers['l1'] = loss; layers['l2'] = loss2; layers['loss'] = loss
                correct_prediction = tf.equal(self.label, tf.argmax(softmax_linear, axis=1))
                accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
                layers['accuracy'], layers['correct'] = accuracy, correct_prediction
                self.layers = layers
            elif method == 'tflearn':
                pass
            #loss = tflearn.regression(
            pass

    def _build_train_op(self, loss):
        """
        not currently used.
        """
        with self.graph.as_default():
            opt = tf.train.RMSPropOptimizer(1e-3).minimize(loss)
            return opt

    def _l2_loss(self, loss, alpha=1e-4):
        """
        weight decay same as in paper.
        """
        with self.graph.as_default():
            result = []
            for v in self.graph.get_collection("L2"):
                result.append(alpha * tf.nn.l2_loss(v))
            l2_loss = loss + tf.add_n(result, name="regularized_weights")
            return l2_loss

    def debug_init(self, sess):
        with self.graph.as_default():
            writer = tf.summary.FileWriter("logs", sess.graph)
            init = tf.global_variables_initializer()
            sess.run(init)
            writer.close()

    def train(self, X, Y, sess, num_epoches=10, report_every=20):
        logit = self.layers['logit']
        accuracy = self.layers['accuracy']
        correct = self.layers['correct']
        loss = self.layers['l2_loss']
        train_op = self.layers['optimizer']
        # print the graph on initiailization
        self.debug_init(sess)
        acc_trajectory, loss_trajectory = [], []
        test_acc_trajectory = []
        for epoch in range(num_epoches):
            counter = 0
            tflearn.is_training(True, session=sess)
            for xb, yb in zip(X, Y):
                #print("xbatch {}, y batch {}".format(np.linalg.norm(xb), np.linalg.norm(yb)))
                sess.run(train_op, feed_dict={self.image:np.transpose(xb, [0, 2, 3, 1]),\
                        self.label:yb})
                counter += 1
                if counter % report_every == 0:
                    acc, obj = sess.run([accuracy, loss], feed_dict={self.image:np.transpose(xb, [0, 2, 3, 1]),\
                            self.label:yb})
                    acc_trajectory.append(acc)
                    loss_trajectory.append(obj)
            # now running on testing
            tflearn.is_training(False, session=sess)
            num_correct, total = 0, 0
            for xb, yb in zip(X, Y):
                correct_guess = sess.run(correct, feed_dict={self.image:np.transpose(xb, [0, 2, 3, 1]),\
                        self.label:yb})
                num_correct += np.sum(correct_guess)
                total += len(xb)
                #print("{}, num_correct is {}, total is {}".format(correct_guess[:3], num_correct, total))
            test_accuracy = num_correct/total
            print("epoch {} done, training set accuracy is {}".format(epoch, test_accuracy))
            test_acc_trajectory.append(test_accuracy)
        return test_acc_trajectory, acc_trajectory, loss_trajectory

    def predict(self, data_feeder, sess):
        #writer = tf.summary.FileWriter("logs", sess.graph)
        correct = self.layers['correct']
        X, Y = data_feeder.get_data("validating")
        num_correct, total = 0, 0
        tflearn.is_training(False, session=sess)
        for xb, yb in zip(X, Y):
            answer = sess.run(correct, feed_dict={self.image:np.transpose(xb, [0, 2, 3, 1]),\
                    self.label:yb})
            num_correct += np.sum(answer)
            total += len(xb)
        return num_correct/total

    def _evaluate():
        pass
