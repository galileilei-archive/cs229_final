import tensorflow as tf 
import numpy as np


class Config(object):
    """docstring for Config"""
    def __init__(self, **kwargs):
        # set default params
        self.network_params = {"num_filter_widths": 4, "num_filter_heights": 4, "num_num_filters": 4,\
                "num_stride_widths": None, "num_stride_heights": None, "set_anchor_point": False, "max_num_layers": 6}
        # alpha is the moving average parameter
        self.train_params = {"hidden_dim": 35, "lr": 0.001, "l2": 0.005, "num_models": 8, "alpha": 0.5} # figure out how to use adam or rmsprop later
        for key in kwargs:
            if key in self.network_params:
                self.network_params[key] = kwargs[key]
            elif key in self.train_params:
                self.train_params[key] = kwargs[key]
            else:
                print("{} is not a usable parameter".format(key))


def model_ix_padding(model, config):
    if model.shape[1] == 3:
        return model

    if model.shape[1] == 4:
        if config.network_params["set_anchor_point"] is True:
            new_model = np.insert(model, [3,4], -1, axis=1)
            return new_model
        if config.network_params.num_stride_heights is not None:
            new_model = np.insert(model, 4, -1, axis=1)
            return new_model
        if config.network_params.num_stride_widths is not None:
            new_model = np.insert(model, 3, -1, axis=1)
            return new_model
    if model.shape[1] == 5:
        if config.network_params["set_anchor_point"] is True:
            if config.network_params["num_stride_heights"] is not None:
                new_model = np.insert(model, 4, -1, axis=1)
                return new_model
            if config.network_params["num_stride_widths"] is not None:
                new_model = np.insert(model, 3, -1, axis=1)
                return new_model
    return model



def new_image_size(cur_image_size, filter_param, stride_param, padding='SAME'):
    # default is SAME padding, only other possibility is valid padding
    if padding == 'VALID':
        height = math.ceil((cur_image_size[0] - filter_param[0] + 1)/stride_param[1])
        width = math.ceil((cur_image_size[1] - filter_param[1] + 1)/stride_param[2])
    else:
        height = math.ceil(cur_image_size[0]/stride_param[1])
        width = math.ceil(cur_image_size[1]/stride_param[2])
    depth = filter_param[3]
    return [height, width, depth]


def model_ix_to_param(model, filter_heights=[1,3,5,7], filter_widths=[1,3,5,7], num_filters=[24,36,48,64], stride_heights=None, stride_widths=None, image_size=[32, 32, 3]):
    assert 'numpy' in str(type(model)), "model should be a numpy array"
    assert model.shape[1] >= 3, "each layer in the model should have at least 3 changeable parameters"
    num_layer_params = model.shape[1]
    cnn_params = dict()
    last_num_filters = image_size[2]
    cur_image_size = image_size
    cur_input = None
    for t in range(model.shape[0]):
        layer_name = "layer_" + str(t+1)
        cnn_params[layer_name] = default_parameters('conv2d')
        layer_ixes = model[t] # layer related ix
        assert (layer_ixes[0] < len(filter_heights)) & (layer_ixes[1] < len(filter_widths)) & (layer_ixes[2] < len(num_filters)), "filter index out of range!"
        filter_param = [filter_heights[layer_ixes[0]], filter_widths[layer_ixes[1]], last_num_filters, num_filters[layer_ixes[2]]] # height, width, in_channels, out_channels
        last_num_filters = num_filters[layer_ixes[2]]
        cnn_params[layer_name]['filter'] = filter_param
        cnn_params[layer_name]['bias'] = [cnn_params[layer_name]['filter'][3]]
        stride_param = cnn_params[layer_name]['stride']
        # stride_param[1] = 2; stride_param[2] = 2  #set default stride width and height as 2 # stride is NHWC (num of batch, height, width, channels)
        if model.shape[1] >= 5:
            assert layer_ixes[3] < len(stride_heights) & layer_ixes[4] < len(stride_widths), "stride index out of range!"
            if layer_ixes[3] >= 0:
                stride_param[1] = stride_heights[layer_ixes[3]]
            if layer_ixes[4] >= 0:
                stride_param[2] = stride_widths[layer_ixes[4]]
        cnn_params[layer_name]['stride'] = stride_param
        cnn_params[layer_name]['input'] = cur_input
        cur_image_size = new_image_size(cur_image_size, filter_param, stride_param)
        print(cur_image_size)
        cur_input = layer_name

    cnn_params['linear'] = default_parameters('linear')

    cnn_params['linear']['weight'][0] = cur_image_size[0] * cur_image_size[1] * cur_image_size[2]
    cnn_params['linear']['input'] = cur_input

    return cnn_params

class RLRNN(object):
    """docstring for RLRNN"""
    def __init__(self, config, graph=None, scope="RLRNN"):
        self.config = config
        self.graph = tf.Graph() if graph is None else graph
        self.scope = scope
        self.hidden_layers_dim, self.output_layers_dim = self.calc_layers_dim(self.config)
        self.baseline = tf.constant(0.0)

        self.bias_adjust = 0.10

        self.add_placeholder()
        self.add_variable()

        if self.config.network_params["set_anchor_point"]:
            self.probs, self.anchors = self.add_model()
            self.loss = self.calc_loss_with_reg(self.probs, self.anchors)
        else:
            self.probs = self.add_model()
            self.loss = self.calc_loss_with_reg(self.probs)
        # # print(tf.shape(self.probs))
        self.train_op = self.add_train_op(self.loss) # need to revise


    def calc_layers_dim(self, config):
        hidden_dim = config.train_params["hidden_dim"]
        # order is filter height, width, num of filters

        hidden_layers_dim = [hidden_dim, hidden_dim] # filter height, width
        output_layers_dim = [config.network_params["num_filter_heights"], config.network_params["num_filter_widths"]]

        if config.network_params["num_stride_heights"] is not None: # stride
            hidden_layers_dim.append(hidden_dim)
            output_layers_dim.append(config.network_params["num_stride_heights"])

        if config.network_params["num_stride_widths"] is not None: # stride
            hidden_layers_dim.append(hidden_dim)
            output_layers_dim.append(config.network_params["num_stride_widths"])

        hidden_layers_dim.append(hidden_dim) # number of filters
        output_layers_dim.append(config.network_params["num_num_filters"])

        return hidden_layers_dim, output_layers_dim


    def add_placeholder(self):
        train_params, network_params = self.config.train_params, self.config.network_params
        with self.graph.as_default(): 
            self.R_placeholder = tf.placeholder(dtype=tf.float32, shape=(train_params["num_models"],))
            self.models_placeholder = tf.placeholder(dtype=tf.int32, shape=(train_params["num_models"], network_params["max_num_layers"], len(self.hidden_layers_dim)))
            if self.config.network_params["set_anchor_point"]:
                self.anchor_placeholder = tf.placeholder(dtype=tf.int32, shape=(train_params["num_models"], network_params["max_num_layers"], network_params["max_num_layers"]))


    def add_variable(self):
        hidden_layers_dim, output_layers_dim = self.hidden_layers_dim, self.output_layers_dim
        assert len(hidden_layers_dim) == len(output_layers_dim), "lengths of hidden_layers_dim and output_layers_dim must agree"

        Ws = list(); bs = list()
        # with tf.variable_scope("RLRNN"):
        with self.graph.as_default(), tf.variable_scope(self.scope):
            for i in range(len(hidden_layers_dim)):
                curW = list()
                # first hidden layer
                Whid1x = tf.get_variable("Whid1x" + str(i), shape=(output_layers_dim[i-1], hidden_layers_dim[i]))
                Whid1h = tf.get_variable("Whid1h" + str(i), shape=(hidden_layers_dim[i-1], hidden_layers_dim[i]))

                # second hidden layer
                Whid2h = tf.get_variable("Whid2h" + str(i), shape=(hidden_layers_dim[i], hidden_layers_dim[i]))
                Whid2g = tf.get_variable("Whid2g" + str(i), shape=(hidden_layers_dim[i-1], hidden_layers_dim[i]))

                # output layer
                Wout = tf.get_variable("Wout" + str(i), shape=(hidden_layers_dim[i], output_layers_dim[i]))
                bout = tf.get_variable("bout" + str(i), shape=(1, output_layers_dim[i]))

                Ws.append([[Whid1x, Whid1h], [Whid2h, Whid2g], [Wout]])
                bs.append([[], [], [bout]])
                

    def add_model(self):
        hidden_layers_dim, output_layers_dim = self.hidden_layers_dim, self.output_layers_dim
        Ws = list(); bs = list()
        # with tf.variable_scope("RLRNN", reuse=True):
        with self.graph.as_default(), tf.variable_scope(self.scope, reuse=True):
            for i in range(len(hidden_layers_dim)):
                curW = list()
                # first hidden layer
                Whid1x = tf.get_variable("Whid1x" + str(i))
                Whid1h = tf.get_variable("Whid1h" + str(i))

                # second hidden layer
                Whid2h = tf.get_variable("Whid2h" + str(i))
                Whid2g = tf.get_variable("Whid2g" + str(i))

                # output layer
                Wout = tf.get_variable("Wout" + str(i))
                bout = tf.get_variable("bout" + str(i))

                Ws.append([[Whid1x, Whid1h], [Whid2h, Whid2g], [Wout]])
                bs.append([[], [], [bout]])


            # all x are probability, so initialized by equal probability, the hidden state h and g is started from 0
            x0, h0, g0 = tf.ones((1, output_layers_dim[-1]))/output_layers_dim[-1], tf.zeros((1, hidden_layers_dim[-1])), tf.zeros((1, hidden_layers_dim[-1]))

            # xs is input, hs in the first hidden layer and gs is the second hidden layer
            # xs, hs and gs are all lists of list, xs[0][0] is numpy array, represents the probability of output
            xs = [[x0]]; hs = [[h0]]; gs = [[g0]]
            max_num_layers = self.config.network_params["max_num_layers"]

            for t in range(1, max_num_layers+1):
                curx, curh, curg = [xs[t-1][-1]], [hs[t-1][-1]], [gs[t-1][-1]]
                for i in range(len(hidden_layers_dim)):
                    prevx, prevh, prevg = curx[i], curh[i], curg[i]

                    h = tf.tanh(tf.matmul(prevx, Ws[i][0][0]) + tf.matmul(prevh, Ws[i][0][1]))
                    g = tf.tanh(tf.matmul(h, Ws[i][1][0]) + tf.matmul(prevg, Ws[i][1][1]))

                    out = tf.matmul(g, Ws[i][2][0]) + bs[i][2][0]
                    x = tf.nn.softmax(out) #output is the probability
                    # x = tf.transpose(tf.nn.softmax(tf.transpose(out)))

                    curx.append(x); curh.append(h); curg.append(g)

                xs.append(curx[1:]); hs.append(curh[1:]); gs.append(curg[1:])

            xs = xs[1:]; hs = hs[1:]; gs = gs[1:]
            return xs



    def sample_ix(self, prob):
        cumprob = np.cumsum(np.array(prob))
        s = np.random.uniform()
        ix = np.argmax(cumprob > s)
        return ix


    def generate_model(self, session, num_models):
        probs = session.run(self.probs)
        models = list()
        for m in range(num_models):
            model_ixes = list()
            for t in range(len(probs)):
                layer_probs = probs[t]
                ixes = list()
                for i in range(len(layer_probs)):
                    cur_prob = layer_probs[i][0]
                    ix = self.sample_ix(cur_prob)
                    ixes.append(ix)
                model_ixes.append(ixes)
            models.append(np.array(model_ixes)) # each model is an array of array, each element represents an index
            # models.append(model_ixes) # each model is a list of list, each element represents an index
        return models


    def calc_loss(self, probs, anchor_probs=None):
        with self.graph.as_default():
            total_loss = tf.constant(0.0)
            num_models = self.config.train_params["num_models"]
            # self.eval_loss_hist = list()
            # self.tmp_loss_hist = list()
            for m in range(num_models):
                eval_loss = tf.constant(0.0)
                for t in range(len(probs)): # len(probs) is the same as max_num_layers
                    for i in range(len(self.hidden_layers_dim)):
                        eval_loss = eval_loss + tf.log(probs[t][i][0][self.models_placeholder[m, t, i]])


                if self.config.network_params["set_anchor_point"] and anchor_probs is not None:
                    for t in range(1, len(probs)):
                        for s in range(t):
                            tmp_loss = tf.cond(tf.equal(self.anchor_placeholder[m, s, t], tf.constant(1)), # from s layer to t layer
                                lambda: anchor_probs[t-1][s][0][0], # anchor prob: the (t-1)-th element is the anchor prob to t layer
                                lambda: tf.constant(1.0) - anchor_probs[t-1][s][0][0])
                            # self.tmp_loss_hist.append(tmp_loss)
                            eval_loss = eval_loss + tmp_loss

                eval_loss = eval_loss * (self.R_placeholder[m] - self.bias_adjust)
                # self.eval_loss_hist.append(eval_loss)

                total_loss = total_loss + eval_loss

            total_loss = -total_loss/num_models

            return total_loss


    def calc_loss_with_reg(self, probs, anchor_probs=None):
        if self.config.network_params["set_anchor_point"] and anchor_probs is not None: 
            total_loss = self.calc_loss(probs, anchor_probs)
        else:
            total_loss = self.calc_loss(probs)

        l2 = self.config.train_params["l2"]
        with self.graph.as_default():
            for v in tf.trainable_variables():
                if "bout" not in v.name:
                    total_loss = total_loss + l2 * tf.nn.l2_loss(v)

            return total_loss



    def add_train_op(self, loss):
        with self.graph.as_default():
            lr = self.config.train_params["lr"]
            # update the bias adjust term (adjust the reward)
            alpha = self.config.train_params['alpha']
            self.bias_adjust = tf.reduce_mean(self.R_placeholder) * alpha + self.bias_adjust * (1 - alpha)
            # print("current total loss is {}".format(total_loss))
            # optimizer = tf.train.AdamOptimizer(lr) # can choose other optimizer
            # optimizer = tf.train.GradientDescentOptimizer(lr)
            optimizer = tf.train.RMSPropOptimizer(lr)
            return optimizer.minimize(loss)


# class RLRNN(object):
# 	"""docstring for RLRNN"""
# 	def __init__(self, config, graph=None, scope="RLRNN"):
# 		self.config = config
# 		self.graph = tf.Graph() if graph is None else graph
# 		self.hidden_layers_dim, self.output_layers_dim = self.calc_layers_dim(self.config)
# 		self.baseline = tf.constant(0.0)
# 		self.add_placeholder()
# 		self.add_variable()
# 		self.probs = self.add_model()
# 		# # print(tf.shape(self.probs))
# 		self.train_op = self.add_train_op(self.probs) # need to revise


# 	def calc_layers_dim(self, config):
# 		hidden_dim = config.train_params["hidden_dim"]
# 		# order is filter height, width, num of filters

# 		hidden_layers_dim = [hidden_dim, hidden_dim] # filter height, width
# 		output_layers_dim = [config.network_params["num_filter_heights"], config.network_params["num_filter_widths"]]

# 		if config.network_params["num_stride_heights"] is not None: # stride
# 			hidden_layers_dim.append(hidden_dim)
# 			output_layers_dim.append(config.network_params["num_stride_heights"])

# 		if config.network_params["num_stride_widths"] is not None: # stride
# 			hidden_layers_dim.append(hidden_dim)
# 			output_layers_dim.append(config.network_params["num_stride_widths"])

# 		hidden_layers_dim.append(hidden_dim) # number of filters
# 		output_layers_dim.append(config.network_params["num_num_filters"])

# 		return hidden_layers_dim, output_layers_dim


# 	def add_placeholder(self):
# 		train_params, network_params = self.config.train_params, self.config.network_params
# 		self.R_placeholder = tf.placeholder(dtype = tf.float32, shape = (train_params["num_models"],))
# 		self.models_placeholder = tf.placeholder(dtype = tf.int32, shape = (train_params["num_models"], network_params["max_num_layers"], 
# 			len(self.hidden_layers_dim)))


	
# 	def add_variable(self):
# 		hidden_layers_dim, output_layers_dim = self.hidden_layers_dim, self.output_layers_dim
# 		assert len(hidden_layers_dim) == len(output_layers_dim), "lengths of hidden_layers_dim and output_layers_dim must agree"

# 		Ws = list(); bs = list()
# 		with tf.variable_scope("RLRNN"):
# 			for i in range(len(hidden_layers_dim)):
# 				curW = list()
# 				# first hidden layer
# 				Whid1x = tf.get_variable("Whid1x" + str(i), shape=(output_layers_dim[i-1], hidden_layers_dim[i]))
# 				Whid1h = tf.get_variable("Whid1h" + str(i), shape=(hidden_layers_dim[i-1], hidden_layers_dim[i]))

# 				# second hidden layer
# 				Whid2h = tf.get_variable("Whid2h" + str(i), shape=(hidden_layers_dim[i], hidden_layers_dim[i]))
# 				Whid2g = tf.get_variable("Whid2g" + str(i), shape=(hidden_layers_dim[i-1], hidden_layers_dim[i]))

# 				# output layer
# 				Wout = tf.get_variable("Wout" + str(i), shape=(hidden_layers_dim[i], output_layers_dim[i]))
# 				bout = tf.get_variable("bout" + str(i), shape=(1, output_layers_dim[i]))

# 				Ws.append([[Whid1x, Whid1h], [Whid2h, Whid2g], [Wout]])
# 				bs.append([[], [], [bout]])





# 	def add_model(self):
# 		hidden_layers_dim, output_layers_dim = self.hidden_layers_dim, self.output_layers_dim
# 		Ws = list(); bs = list()
# 		with tf.variable_scope("RLRNN", reuse=True):
# 			for i in range(len(hidden_layers_dim)):
# 				curW = list()
# 				# first hidden layer
# 				Whid1x = tf.get_variable("Whid1x" + str(i))
# 				Whid1h = tf.get_variable("Whid1h" + str(i))

# 				# second hidden layer
# 				Whid2h = tf.get_variable("Whid2h" + str(i))
# 				Whid2g = tf.get_variable("Whid2g" + str(i))

# 				# output layer
# 				Wout = tf.get_variable("Wout" + str(i))
# 				bout = tf.get_variable("bout" + str(i))

# 				Ws.append([[Whid1x, Whid1h], [Whid2h, Whid2g], [Wout]])
# 				bs.append([[], [], [bout]])


# 		# all x are probability, so initialized by equal probability, the hidden state h and g is started from 0
# 		x0, h0, g0 = tf.ones((1, output_layers_dim[-1]))/output_layers_dim[-1], tf.zeros((1, hidden_layers_dim[-1])), tf.zeros((1, hidden_layers_dim[-1]))
		
# 		# xs is input, hs in the first hidden layer and gs is the second hidden layer
# 		# xs, hs and gs are all lists of list, xs[0][0] is numpy array, represents the probability of output
# 		xs = [[x0]]; hs = [[h0]]; gs = [[g0]]
# 		max_num_layers = self.config.network_params["max_num_layers"]

# 		for t in range(1, max_num_layers+1):
# 			curx, curh, curg = [xs[t-1][-1]], [hs[t-1][-1]], [gs[t-1][-1]]
# 			for i in range(len(hidden_layers_dim)):
# 				prevx, prevh, prevg = curx[i], curh[i], curg[i]

# 				h = tf.tanh(tf.matmul(prevx, Ws[i][0][0]) + tf.matmul(prevh, Ws[i][0][1]))
# 				g = tf.tanh(tf.matmul(h, Ws[i][1][0]) + tf.matmul(prevg, Ws[i][1][1]))

# 				out = tf.matmul(g, Ws[i][2][0]) + bs[i][2][0]
# 				x = tf.nn.softmax(out) #output is the probability
# 				# x = tf.transpose(tf.nn.softmax(tf.transpose(out)))
				
# 				curx.append(x); curh.append(h); curg.append(g)

# 			xs.append(curx[1:]); hs.append(curh[1:]); gs.append(curg[1:])

# 		xs = xs[1:]; hs = hs[1:]; gs = gs[1:]
# 		return xs


# 	def sample_ix(self, prob):
# 		cumprob = np.cumsum(np.array(prob))
# 		s = np.random.uniform()
# 		ix = np.argmax(cumprob>s)
# 		return ix


# 	def generate_model(self, session, num_models):
# 		probs = session.run(self.probs)
# 		models = list()
# 		for m in range(num_models):
# 			model_ixes = list()
# 			for t in range(len(probs)):
# 				layer_probs = probs[t]
# 				ixes = list()
# 				for i in range(len(layer_probs)):
# 					cur_prob = layer_probs[i][0]
# 					ix = self.sample_ix(cur_prob)
# 					ixes.append(ix)
# 				model_ixes.append(ixes)
# 			models.append(np.array(model_ixes)) # each model is an array of array, each element represents an index
# 			# models.append(model_ixes) # each model is a list of list, each element represents an index
# 		return models


# 	# def calc_loss(self, probs):
# 	# 	total_loss = tf.constant(0.0)
# 	# 	num_models = self.config.train_params["num_models"]
# 	# 	for m in range(num_models):
# 	# 		eval_ix = list()
# 	# 		for t in range(len(probs)): # len(probs) is the same as max_num_layers
# 	# 			for i in range(len(self.hidden_layers_dim)):
# 	# 				eval_ix.append([t, i, self.models_placeholder[m, t, i]])
# 	# 		eval_probs = tf.gather_nd(probs, eval_ix)
# 	# 		eval_log_probs = tf.log(eval_probs)
# 	# 		eval_loss = tf.reduce_sum(eval_log_probs) * self.R_placeholder[m]
# 	# 		total_loss += eval_loss


# 	# 	# total_loss = -total_loss/tf.constant(num_models)	
# 	# 	total_loss = -total_loss/num_models

# 	# 	return total_loss


# 	def calc_loss(self, probs):
# 		total_loss = tf.constant(0.0)
# 		num_models = self.config.train_params["num_models"]
# 		for m in range(num_models):
# 			eval_loss = tf.constant(0.0)
# 			for t in range(len(probs)): # len(probs) is the same as max_num_layers
# 				for i in range(len(self.hidden_layers_dim)):
# 					eval_loss += tf.log(probs[t][i][0][self.models_placeholder[m, t, i]])
# 			eval_loss *= self.R_placeholder[m]
# 			total_loss += eval_loss

# 		total_loss = -total_loss/num_models

# 		return total_loss




# 	def calc_loss_with_reg(self, probs):
# 		total_loss = self.calc_loss(probs)

# 		l2 = self.config.train_params["l2"]
# 		for v in tf.trainable_variables():
# 			total_loss += l2 * tf.nn.l2_loss(v) 

# 		return total_loss


# 	def add_train_op(self, probs):
# 		lr = self.config.train_params["lr"]
# 		total_loss = self.calc_loss_with_reg(probs)
# 		optimizer = tf.train.AdamOptimizer(lr) # can choose other optimizer
# 		return optimizer.minimize(total_loss)

		
