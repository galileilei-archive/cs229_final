# CS229 Final Project

This project implements single GPU version of [NEURAL ARCHITECTURE SEARCH WITH REINFORCEMENT LEARNING](https://arxiv.org/pdf/1611.01578.pdf) with TFLearn framework. The code is avaiable at <https://bitbucket.org/galileilei/cs229_final> and the project report at <https://www.overleaf.com/12822137mvgvfzbctfzb>.

## What files are used.

There are many experiemental files. The following code are used for the
figures and data in the final report.

### LSTM Controller Part

1. `cs231n/data_feeder.py` : DataFeeder() and Batch() class

2. `cs231n/models/cnn.py` : HyperParameter() and CNN() class

3. `RLRNN.py` : RLRNN() class, implemented a two-layer RNN controller

4. `RLLSTM.py` : RLLSTM() class, implemented a two-layer LSTM controller

5. `RLLSTM_anchor.py`: RLLSTM() class, implemented a two-layer LSTM controller with skip connection

6. `test_RLLSTM_anchor-simulation.ipynb`: test RLLSTM with simulation data on linear bandit problem

7. `baseline_parametric.py`: BaseModel() class, model reward as a parametric function of all hyperparameters in all layers

8. `baseline_NN.py`: BaseNNModel() class, model reward as the output of a neural network with all hyperparameters in all layers as inputs. Number of hidden layers is flexible.

9. `test_baseline_parametric_model.ipynb`: test BaseModel class with simulation dafa on linear bandit problem. Correlation between predicted and true rewards is reported. The result of regressing predicted rewards on hyperparameters is reported (all coefficients should be positive)

10. `test_baseline_NN_model.ipynb`: similar test on BaseNNModel as the test performed on BaseModel in point 9

### CNN Part

1. `src/cnn_mnist.py`: convolutional network on MNIST dataset using
   TFLearn.

2. `src/cnn_cifar10.py`: convolutional network on CIFAR10 dataset using
   TFLearn.

3. `src/random_search_controller.py`: random search strategy to tune
   hyperparameter.

4. `src/lstm_controller.py`: LSTM controller to tune hyperparameters.

5. `cnn_visualization.ipynb`: generates Figure 3, 4, 5 in the final
   report.

### Misc:

* `tensorflow_review.ipynb` : running multiple computational graph instances.

* `rnn_lstm_cnn.ipynb` : experimental code, running LSTM controller with CNN network.

* `constructing_graph.ipynb` : experimental code, constructing a DAG instead of stacked/layered neural network.

* `comparing different gradient update.ipynb` : experimental code, effect of high dropout rate on various gradient update scheme
