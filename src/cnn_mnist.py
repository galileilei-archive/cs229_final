"""
File: cnn_mnist.py
Author: Me
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    sample a cnn with custom skip connection,
    return its validation accuracy on a test dataset
"""

import tflearn
import tensorflow as tf

from layer_util import Node, Graph, default_config
from layer import build_graph # terminal does not work with relative import
#from .layer import build_graph # notebook requires relative import


import tflearn.datasets.mnist as mnist

def load_mnist():
    X, Y, testX, testY = mnist.load_data(one_hot=True)
    X = X.reshape([-1, 28, 28, 1])
    testX = testX.reshape([-1, 28, 28, 1])
    return X, Y, testX, testY

def sample_CNN(graph, X, Y, testX, testY):
    #TODO: add memoization (need to find hemorphic graph structure)
    with tf.Graph().as_default():
        image = tflearn.layers.core.input_data(shape=[None, 28, 28, 1], name='input')
        net, layers = build_graph(image, graph)
        net = tflearn.layers.estimator.regression(net, optimizer='adam', learning_rate=0.01,
                loss='categorical_crossentropy', name='target')

        model = tflearn.DNN(net, tensorboard_verbose=0)#, tensorboard_dir='ckpt/mnist_logs') for efficiency, do not record anything
        model.fit({'input':X}, {'target':Y}, n_epoch=2,
                validation_set=({'input':testX}, {'target':testY}),
                show_metric=True)

        acc = model.evaluate({'input':X}, {'target':Y})
    return acc

if __name__ == "__main__":
    X, Y, testX, testY = load_mnist()
    a = Node("a", default_config('conv2d'))
    b = Node("b", default_config('conv2d'))
    c = Node("c", default_config('conv2d'))
    d = Node("d", default_config('conv2d'))
    e = Node("linear", default_config('linear'))
    edges = [("a", "b"), ("b", "c"), ("a", "d"), ("b", "d")]
    g = Graph([a, b, c, d, e], edges);
    acc = sample_CNN(g, X, Y, testX, testY)
    print(acc)
