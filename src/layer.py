"""
File: layer.py
Author: Me
Email: yourname@email.com
Github: https://github.com/yourname
Description: 
    build a layer using key:value dictionary
"""

import numpy as np
import tflearn
import tensorflow as tf
from copy import deepcopy

def conv2d(x, scope, **kwargs):
    " using tflearn to build "
    _kwargs = deepcopy(kwargs)
    use_bn = _kwargs.pop('use_bn')
    max_pool = _kwargs.pop('max_pool')
    with tf.variable_scope(scope):
        x = tflearn.layers.conv.conv_2d(x, **_kwargs)
        if use_bn:
            x = tflearn.layers.normalization.batch_normalization(x)
        if max_pool is not None:
            x = tflearn.layers.conv.max_pool_2d(x, max_pool)
        return x

def linear(x, scope, **kwargs):
    " using tflearn to build "
    with tf.variable_scope(scope):
        x = tflearn.layers.core.fully_connected(x, **kwargs)
        return x

def concat(inputs, scope):
    """
    return a 4D tensor concat along the last dimension.
    :param inputs: a list of 4D tensors, where shape[1] and shape[2] may not match.
    """
    print("inputs to concat_input() are {}".format(inputs))
    if not isinstance(inputs, list):
        return inputs
    all_h = [v.shape[1].value for v in inputs]
    all_w = [v.shape[2].value for v in inputs]
    output_h = np.max(all_h)
    output_w = np.max(all_w)
    padded_inputs = []
    with tf.variable_scope(scope):
        for i, v in enumerate(inputs):
            h, w = v.shape[1].value, v.shape[2].value
            pad_h, pad_w =int((output_h - h)/2), int((output_w - w)/2)
            eps_h = output_h - h - 2 * pad_h
            eps_w = output_w - w - 2 * pad_w
            print((pad_h, pad_w, h, w, output_h, output_w))
            padding = np.array([[0, 0], [pad_h + eps_h, pad_h], [pad_w + eps_w, pad_w], [0, 0]])
            print(tf.pad(v, padding).shape)
            padded_inputs.append(tf.pad(v, padding))
        result = tf.concat(padded_inputs, 3) # concat along the last dimension
        return result

def build_graph(incoming, graph):
    layers = dict() # to be populated
    for i, tensor in enumerate(graph.order):
        print("build node {}".format(tensor))
        inputs = graph.get_inputs(tensor)
        if len(inputs) == 0:
            incoming = incoming
        else:
            incoming = concat([layers[x] for x in inputs], scope=tensor)
        layers[tensor] = conv2d(incoming, scope=tensor, **graph.get_config(tensor))
        layers["{}_anchor".format(tensor)] = incoming
        outputs = graph.get_outputs(tensor)
        if len(outputs) == 0:
            graph.get_inputs("linear").add(tensor)
    # build the last linear layers
    inputs = graph.get_inputs("linear")
    incoming = concat([layers[x] for x in inputs], scope=tensor)
    softmax_linear = linear(incoming, scope=tensor, **graph.get_config("linear"))
    layers['linear'] = softmax_linear
    layers['linear_anchor'] = incoming
    return softmax_linear, layers
