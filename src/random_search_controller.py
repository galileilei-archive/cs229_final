import numpy as np
import tensorflow as tf
import tflearn
from layer_util import Node, Graph, default_config
from layer import build_graph
from cnn_mnist import load_mnist#, sample_CNN
from cnn_cifar10 import load_cifar10, sample_CNN
from lstm import RLLSTM, Config

LSTM_CONFIG = {
        'num_models' : 20,
        'max_num_layers' : 3,
        'num_filter_heights' : 4,
        'num_filter_widths' : 4,
        'num_num_filters' : 4,
        'lr' : 0.1,
        'l2' : 0.0001,
        'filter_heights' : [1,3,5,7],
        'filter_widths' : [1,3,5,7],
        'num_filters' : [24,36,48,64],
        'set_anchor_point' : True
        }

def random_search_graph():
    """
    assume each choice is made uniformly at random
    """
    num_layer = LSTM_CONFIG['max_num_layers']
    num_choices = 4 #TODO: assume always take 4 choices

    vertices = list()
    for i in range(num_layer):
        idx = np.random.randint(num_choices, size=3)
        h = LSTM_CONFIG['filter_heights'][idx[0]]
        w = LSTM_CONFIG['filter_widths'][idx[1]]
        nb = LSTM_CONFIG['num_filters'][idx[2]]
        config = default_config()
        config['filter_size'] = [h, w]
        config['nb_filter'] = nb
        n = Node(str(i), config)
        vertices.append(n)

    edges = list()
    for j in range(num_layer):
        for i in range(j):
            if np.random.uniform() > 0.5:
                edges.append((str(i), str(j)))

    vertices.append(Node("linear", default_config('linear')))
    graph = Graph(vertices, edges)
    return graph

if __name__ == "__main__":
    # load MNIST data
    #X, Y, testX, testY = load_mnist()
    X, Y, testX, testY = load_cifar10()

    # just random sample
    for i in range(400):
        g = random_search_graph()
        acc = sample_CNN(g, X, Y, testX, testY)
        print("random search step {}, acc is {}".format(i, acc))
