"""
File: layer_util.py
Author: Me
Email: yourname@email.com
Github: https://github.com/yourname
Description:
    layers key-value parameters
"""
import tensorflow as tf
import numpy as np
from collections import defaultdict
from copy import deepcopy

class Node:
    def __init__(self, _name, _config={}):
        self.name = _name
        self.config = _config

class Graph:
    def __init__(self, nodes, edges):
        self.config = dict()
        self.V = [n.name for n in nodes]
        for n in nodes:
            self.config[n.name] = n.config
        self.inputs = defaultdict(set)
        self.outputs = defaultdict(set)
        for e in edges:
            i, j = e
            self.outputs[i].add(j)
            self.inputs[j].add(i)
        self.order = self.topology_sort()

    def get_config(self, name):
        return self.config[name]

    def get_inputs(self, name):
        return self.inputs[name]

    def get_outputs(self, name):
        return self.outputs[name]

    def topology_sort(self):
        inputs = deepcopy(self.inputs)
        outputs = deepcopy(self.outputs)
        # kahn's algorithm
        S = set()
        L = list()
        for v in self.V:
            if v == "linear":
                continue
            if len(inputs[v]) == 0:
                S.add(v)
        while len(S) > 0:
            n = S.pop()
            L.append(n)
            for m in outputs[n]:
                inputs[m].discard(n)
                if len(inputs[m]) == 0:
                    S.add(m)
            outputs[n].clear()
        # check if there is any edge left
        for v in self.V:
            if v == 'linear':
                continue
            if len(inputs[v]) > 0 or len(outputs[v]) > 0:
                return None
        return L

def default_config(layer_type="conv2d"):
    """
    returns a dictionary which describe a 'conv2d' or 'linear' layer.
    the key-value pair follows from tflearn function convention.
    """
    result = {}
    if layer_type == "conv2d":
        result = {'filter_size' : [5, 5], 'nb_filter' : 16,\
                'strides' : [1, 1], 'padding' : 'same',\
                'bias' : True, 'max_pool': [2, 2], 'use_bn' : True,\
                'activation':'relu', 'regularizer':'L2'}
    elif layer_type == "linear":
        result = {'n_units' : 10, 'bias': True, 'regularizer': 'L2',\
                'activation':'softmax'}
    else:
        raise NotImplementedError("unknown layer type is given")
    return result

class HyperParameter:
    """
    Stores all parameters related to CNN.
    """
    def __init__(self, _param, _sorted):
        """
        :param _param : _param[layer_name] = dict() with all infos
        :param _sorted: _sorted = [] is a topologically sorted list of layer_name

        example:
        Suppose the network is a two layer CNN follows by one affine layer:
        'layer_1' -> 'layer_2' -> 'linear'

        Then we declare it as:
        hyperparam = dict()
        hyperparam['layer_1'] = default_parameters("conv2d")
        hyperparam['layer_1']['filter'] = [5, 5, 3, 32] # to be used with cifar-10 data
        hyperparam['layer_2'] = default_parameters("conv2d")
        hyperparam['layer_2']['input'] = 'layer_1'
        hyperparam['linear'] = default_parameters("linear")
        hyperparam['linear']['input'] = 'layer_2'

        cnn_param = HyperParameter(hyperparam, ['layer_1', 'layer_2'])
        cnn = CNN(cnn_param)
        """
        self.parameters = _param
        self.layers_topo_sorted = _sorted
        pass

    def topology_sort(self):
        return self.layers_topo_sorted

    def get(self, i):
        return self.parameters[i]

    def num_layers():
        return len(self.layers_topo_sorted)
