"""
File: cnn_cifar10.py
Author: Me
Email: yourname@email.com
Github: https://github.com/yourname
Description: 
    sample a cnn with custom skip connection,
    return tis validation accuracy on a test dataset
"""
import numpy as np
import tensorflow as tf
import tflearn
import tflearn.datasets.cifar10 as cifar10
from tflearn.data_preprocessing import ImagePreprocessing
from tflearn.data_augmentation import ImageAugmentation

from layer_util import Node, Graph, default_config
from layer import build_graph # terminal does not work with relative import

# Data loading and preprocessing
def load_cifar10():
    (X, Y), (X_test, Y_test) = cifar10.load_data()
    X, Y = tflearn.data_utils.shuffle(X, Y)
    Y = tflearn.data_utils.to_categorical(Y, nb_classes=10)
    Y_test = np.array(Y_test)
    Y_test = tflearn.data_utils.to_categorical(Y_test, nb_classes=10)
    return X, Y, X_test, Y_test

def sample_CNN(graph, X, Y, testX, testY):
    #TODO: add memoization (need to find hemorphic graph structure)
    with tf.Graph().as_default():
        # Real-time data preprocessing
        img_prep = ImagePreprocessing()
        img_prep.add_featurewise_zero_center()
        img_prep.add_featurewise_stdnorm()

        # Real-time data augmentation
        img_aug = ImageAugmentation()
        img_aug.add_random_flip_leftright()
        img_aug.add_random_rotation(max_angle=25.)
        image = tflearn.layers.core.input_data(shape=[None, 32, 32, 3],
                name='input',
                data_preprocessing=img_prep,
                data_augmentation=img_aug)
        net, layers = build_graph(image, graph)
        net = tflearn.layers.estimator.regression(net, optimizer='adam', learning_rate=0.01,
                loss='categorical_crossentropy', name='target')

        model = tflearn.DNN(net, tensorboard_verbose=0)#, tensorboard_dir='ckpt/mnist_logs') for efficiency, do not record anything
        model.fit({'input':X}, {'target':Y}, n_epoch=5,
                validation_set=({'input':testX}, {'target':testY}),
                show_metric=True)

        acc = model.evaluate({'input':X}, {'target':Y})
    return acc

if __name__ == "__main__":
    X, Y, testX, testY = load_cifar10()
    a = Node("a", default_config('conv2d'))
    b = Node("b", default_config('conv2d'))
    c = Node("c", default_config('conv2d'))
    d = Node("d", default_config('conv2d'))
    e = Node("linear", default_config('linear'))
    edges = [("a", "b"), ("b", "c"), ("a", "d"), ("b", "d")]
    g = Graph([a, b, c, d, e], edges);
    acc = sample_CNN(g, X, Y, testX, testY)
    print(acc)
