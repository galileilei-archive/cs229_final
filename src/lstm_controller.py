import numpy as np
import tensorflow as tf
import tflearn
from layer_util import Node, Graph, default_config
from layer import build_graph
from cnn_mnist import load_mnist#, sample_CNN
from cnn_cifar10 import load_cifar10, sample_CNN
from lstm import RLLSTM, Config

LSTM_CONFIG = {
        'num_models' : 20,
        'max_num_layers' : 3,
        'num_filter_heights' : 4,
        'num_filter_widths' : 4,
        'num_num_filters' : 4,
        'lr' : 0.1,
        'l2' : 0.0001,
        'filter_heights' : [1,3,5,7],
        'filter_widths' : [1,3,5,7],
        'num_filters' : [24,36,48,64],
        'set_anchor_point' : True
        }

def sample_discrete(prob):
    """
    given a discrete distribution, prob[i] = probality of choosing i,
    returns a sample drawn from this distribution
    """
    cumprob = np.cumsum(np.array(prob))
    s = np.random.uniform()
    idx = np.argmax(cumprob > s)
    return idx

def sample_graph(probs, anchor_probs):
    """
    given probablity for each action,
    sample a CNN architecture

    returns:
    1. layer_util.Graph() object
    2. RLLSTM.model_placeholder
    3. RLLSTM.anchor_placeholder
    """
    probs = np.array(probs)
    probs = np.squeeze(probs)
    num_layer = probs.shape[0]
    num_choices = probs.shape[1]

    vertices = list()
    model_placeholder = np.zeros((num_layer, num_choices))
    anchor_placeholder = np.zeros((num_layer, num_layer))
    filter_height = LSTM_CONFIG['filter_heights']
    filter_width = LSTM_CONFIG['filter_widths']
    nb_filter = LSTM_CONFIG['num_filters']
    for i in range(num_layer):
        filter_height_prob = probs[i][0]
        filter_width_prob = probs[i][1]
        nb_filter_prob = probs[i][2]
        h_idx = sample_discrete(filter_height_prob)
        w_idx = sample_discrete(filter_width_prob)
        nb_idx = sample_discrete(nb_filter_prob)
        config = default_config()
        config['filter_size'] = [filter_height[w_idx], filter_width[h_idx]]
        config['nb_filter'] = nb_filter[nb_idx]
        n = Node(str(i), config)
        vertices.append(n)
        # construct placeholder
        model_placeholder[i, :] = np.array([h_idx, w_idx, nb_idx])

    edges = list()
    for e in anchor_probs.keys():
        if np.random.uniform() > anchor_probs[e]:
            i, j = e
            edges.append((str(i), str(j)))
            # contruct placeholder
            anchor_placeholder[i, j] = 1

    # add linear layer
    vertices.append(Node("linear", default_config('linear')))
    graph = Graph(vertices, edges)
    return graph, model_placeholder, anchor_placeholder

if __name__ == "__main__":
    # load MNIST data OR Cifar10 data
    #X, Y, testX, testY = load_mnist()
    X, Y, testX, testY = load_cifar10()

    # initialize controller
    config = Config(**LSTM_CONFIG)
    controller = RLLSTM(config)
    sess = tf.Session(graph=controller.graph)
    with controller.graph.as_default():
        init = tf.global_variables_initializer()
    sess.run(init)

    # train controller
    for epoch in range(20):
        probs, anchor_probs = sess.run([controller.probs, controller.anchors])
        num_models = LSTM_CONFIG['num_models']
        num_layers = LSTM_CONFIG['max_num_layers']
        models_incoming = np.zeros((num_models, num_layers, 3))
        anchor_incoming = np.zeros((num_models, num_layers, num_layers))
        reward_incoming = np.zeros((num_models, ))
        for m in range(num_models):
            g, model, anchor = sample_graph(probs, anchor_probs)
            acc = sample_CNN(g, X, Y, testX, testY)
            models_incoming[m] = model
            anchor_incoming[m] = anchor
            reward_incoming[m] = acc[0]
        feed_dict = {
                controller.R_placeholder : reward_incoming,
                controller.models_placeholder : models_incoming,
                controller.anchor_placeholder : anchor_incoming
                }
        _, loss = sess.run([controller.train_op, controller.loss], feed_dict)
        print("lstm epoch {}, loss is {}".format(epoch, loss))
