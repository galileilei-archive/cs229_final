import tensorflow as tf
import numpy as np
import math 
from cs231n.models.cnn import default_parameters
# from RLLSTM import Config


class BaseModel(object):
    """docstring for QuadModel"""
    def __init__(self, config, graph=None, scope="BaseModel", model_order=2):
        self.config = config
        self.graph = tf.Graph() if graph is None else graph
        self.scope = scope
        self.model_order = model_order
        self.input_dim = self.calc_input_dim()

        self.add_placeholder()
        self.add_variable()

        self.R_pred = self.add_model()
        self.loss = self.calc_loss_with_reg(self.R_pred)
        self.train_op = self.add_train_op(self.loss) # need to revise


    def calc_input_dim(self):
        network_params = self.config.network_params
        input_dim = (3 + 1 * (network_params["num_stride_widths"] is not None) + 1 * (network_params["num_stride_heights"] is not None) ) * network_params["max_num_layers"]
        # there are num_layer * (num_layer - 1)/2 possible connections
        if network_params["set_anchor_point"]:
            input_dim  = input_dim + network_params["max_num_layers"] * (network_params["max_num_layers"] - 1)/2

        return input_dim


    def add_placeholder(self):
        with self.graph.as_default(): 
            self.R_placeholder = tf.placeholder(dtype=tf.float32, shape=(None))
            self.X_placeholder = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dim))


    def add_variable(self):
        with self.graph.as_default(), tf.variable_scope(self.scope):
            Ws = list()
            for i in range(self.model_order):
                W = tf.get_variable("W" + str(i), shape = (self.input_dim, 1))
                Ws.append(W)
            b = tf.get_variable("b", shape = (1,))


    def add_model(self):
        with self.graph.as_default(), tf.variable_scope(self.scope, reuse=True):
            Ws = list()
            for i in range(self.model_order):
                W = tf.get_variable("W" + str(i))
                Ws.append(W)
            b = tf.get_variable("b")

            r_pred = tf.matmul(self.X_placeholder ,Ws[0]) + b
            for i in range(1, self.model_order):
                r_pred = r_pred + tf.matmul(tf.pow(self.X_placeholder, (i + 1)), Ws[i])

            # r_pred = tf.sigmoid(r_pred)
            # print(tf.shape(Ws[0]))
            return r_pred


    def calc_loss_with_reg(self, pred):
        # loss = tf.nn.l2_loss(self.R_placeholder - pred)
        # print(tf.shape(pred))
        
        l2 = self.config.train_params["l2"]
        with self.graph.as_default():
            loss = tf.reduce_mean((self.R_placeholder - pred)**2)
            for v in tf.trainable_variables():
                if "b" not in v.name:
                    loss = loss + l2 * tf.nn.l2_loss(v)
            return loss


    def add_train_op(self, loss):
        with self.graph.as_default():
            lr = self.config.train_params["lr"]
            # optimizer = tf.train.RMSPropOptimizer(lr)
            optimizer = tf.train.GradientDescentOptimizer(lr)
            return optimizer.minimize(loss)




