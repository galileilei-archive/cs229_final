import tensorflow as tf
import numpy as np
import math 
from cs231n.models.cnn import default_parameters

class BaseNNModel(object):
    """docstring for QuadModel"""
    def __init__(self, config, graph=None, scope="BaseModel"):
        self.config = config
        self.graph = tf.Graph() if graph is None else graph
        self.scope = scope

        self.input_dim, self.num_hidden_layers = self.calc_dim()

        self.add_placeholder()
        self.add_variable()

        self.R_pred = self.add_model()
        self.loss = self.calc_loss_with_reg(self.R_pred)
        self.train_op = self.add_train_op(self.loss) # need to revise


    def calc_dim(self):
        network_params = self.config.network_params
        input_dim = (3 + 1 * (network_params["num_stride_widths"] is not None) + 1 * (network_params["num_stride_heights"] is not None) ) * network_params["max_num_layers"]
        # there are num_layer * (num_layer - 1)/2 possible connections
        if network_params["set_anchor_point"]:
            input_dim  = input_dim + network_params["max_num_layers"] * (network_params["max_num_layers"] - 1)/2


        hidden_dim = self.config.train_params["hidden_dim"]
        if type(hidden_dim) is list:
            num_hidden_layers = len(hidden_dim)
        else:
            num_hidden_layers = 1
        return input_dim, num_hidden_layers


    def add_placeholder(self):
        with self.graph.as_default(): 
            self.R_placeholder = tf.placeholder(dtype=tf.float32, shape=(None))
            self.X_placeholder = tf.placeholder(dtype=tf.float32, shape=(None, self.input_dim))


    def add_variable(self):
        hidden_dim = self.config.train_params["hidden_dim"]
        if type(hidden_dim) is not list:
            hidden_dim = [hidden_dim]
        prev_dim = self.input_dim
        with self.graph.as_default(), tf.variable_scope(self.scope):
            Ws = list(); bs = list()
            for i in range(self.num_hidden_layers):
                W = tf.get_variable("W" + str(i), shape = (prev_dim , hidden_dim[i]))
                b = tf.get_variable("b" + str(i), shape = (1, hidden_dim[i]))
                prev_dim = hidden_dim[i]
                Ws.append(W); bs.append(b)

            W = tf.get_variable("W" + str(self.num_hidden_layers), shape = (prev_dim , 1))
            b = tf.get_variable("b" + str(self.num_hidden_layers), shape = (1, ))
            Ws.append(W); bs.append(b)


    def add_model(self):
        with self.graph.as_default(), tf.variable_scope(self.scope, reuse=True):
            Ws = list(); bs = list()

            for i in range(self.num_hidden_layers + 1):
                W = tf.get_variable("W" + str(i))
                b = tf.get_variable("b" + str(i))
                Ws.append(W); bs.append(b)

            prev_hidden = self.X_placeholder
            for i in range(self.num_hidden_layers):
                prev_hidden = tf.tanh(tf.matmul(prev_hidden, Ws[i]) + bs[i])

            # r_pred = tf.sigmoid(tf.matmul(prev_hidden ,Ws[self.num_hidden_layers]) + bs[self.num_hidden_layers]) 
            r_pred = tf.matmul(prev_hidden ,Ws[self.num_hidden_layers]) + bs[self.num_hidden_layers] 

            return r_pred


    def calc_loss_with_reg(self, pred):
        
        # loss = tf.reduce_mean(tf.square(self.R_placeholder - pred))
        # loss = loss / tf.shape()
        # loss = tf.nn.l2_loss(self.R_placeholder - pred)
        l2 = self.config.train_params["l2"]
        with self.graph.as_default():
            loss = tf.reduce_mean((self.R_placeholder - pred)**2)
            for v in tf.trainable_variables():
                if "b" not in v.name:
                    loss = loss + l2 * tf.nn.l2_loss(v)
            return loss


    def add_train_op(self, loss):
        with self.graph.as_default():
            lr = self.config.train_params["lr"]
            optimizer = tf.train.RMSPropOptimizer(lr)
            # optimizer = tf.train.GradientDescent(lr)
            return optimizer.minimize(loss)




